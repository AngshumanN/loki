import enum

JSONPATHS = {
    'document': '$.document.[0].elements[*].children[*].children[*].children[*]',
    'domain': '$.domain.[0].children[*].children[*].children[*]'
}

element_type_key_mapping = {"sentence": ("node_type", "text"), "paragraph": ("node_type", "text"),
                            "heading": ("node_type", "text"), "attribute": ("name", "value")}
