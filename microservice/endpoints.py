import traceback

from sanic import Sanic
from sanic.response import json
from sanic_cors import CORS
from sanic.response import json as response_json

from microservice.handlers.rules_handler import RulesHandler

app = Sanic("rules-engine")
CORS(app, automatic_options=True)


@app.route("/")
async def test(request):
    return json({"project": "rule engine", "author": "Angshuman Nandy (xpms)", "message": "rule engine is live!!"})


@app.route("rule/execute", methods=["POST"])
async def execute_rule(request):
    req_json = request.json
    try:
        rule_handler = RulesHandler()
        rule_response = rule_handler.handle_rule_execution(req_json)
        return response_json({"status": {"status_code": 200, "message": "rule processed"}, "data": rule_response})
    except Exception as e:
        return response_json({"status": {"status_code": 500, "message": "rule processing failed"},
                              "data": {"error": str(e), "traceback": traceback.format_exc()}})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=9007)
