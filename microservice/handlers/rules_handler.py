from datetime import datetime

from microservice.models.input_object import InputObject
from microservice.models.rule import Rule
from microservice.utilities.profiler_utils import add_profiling


class RulesHandler(object):

    @staticmethod
    def handle_rule_execution(request):
        inputs = request.get("inputs", None)
        rules = request.get("rules", [])
        input_obj = InputObject(inputs)
        data_to_match = input_obj.get_match_value()

        rule_exec_results = []
        for rule_json in rules:
            rule = Rule(rule_json)
            rule.input_object = input_obj.json_object
            rule_exec_results.append(rule.execute(data_to_match))
            # if rule.execute(data_to_match):
            #     st_time = datetime.now()
            #     rule_exec_results.append((True, rule.apply_action()))
            #     e_time = datetime.now()
            #     print("time taken to apply action :", e_time - st_time)
        return rule_exec_results
