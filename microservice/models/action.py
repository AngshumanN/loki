from copy import deepcopy
from uuid import uuid4

from microservice.models.base_condition_action import BaseConditionAction
from microservice.utilities.domain_utils import get_all_parent_ids, check_parents


class Action(BaseConditionAction):
    match_group = False
    action_type = "domain"
    operator = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.action_type = kwargs.get("action_type", "domain")
        self.match_group = kwargs.get("$match_group", False)
        self.operator = kwargs.get("$op", None)

    def check_parents(self, parent_ids):
        first_id = parent_ids[0]
        for p in parent_ids:
            if p != first_id:
                return False
        return True

    def append_attribute(self, inputs, matches, parent_ids=None):
        inputs = inputs[0] if isinstance(inputs, list) else inputs
        if parent_ids:
            for parent_id in parent_ids:
                for dom in inputs["domain"][0]["children"]:
                    for ent in dom["children"]:
                        if ent["id"] == parent_id:
                            app_val = deepcopy(matches[0])
                            app_val[self.match_Lkey] = self.lvalue
                            app_val[self.match_Rkey] = self.rvalue
                            app_val["id"] = str(uuid4())
                            ent["children"].append(app_val)
            return
        else:
            ent = inputs["domain"][0]["children"][0]["children"][-1]
            parent_id = ent["id"]
            app_val = deepcopy(matches[0])
            app_val[self.match_Lkey] = self.lvalue
            app_val[self.match_Rkey] = self.rvalue
            app_val["parent_id"] = parent_id
            app_val["id"] = str(uuid4())
            ent["children"].append(app_val)
            return

    def perform(self, inputs, matches):
        if self.operator:
            if self.operator.lower() == "create_attribute":
                if self.match_group:
                    parent_ids = get_all_parent_ids(matches)
                    similar_parents = check_parents(parent_ids)
                    if similar_parents:
                        self.append_attribute(inputs, matches, similar_parents)
                else:
                    self.append_attribute(inputs, matches)
