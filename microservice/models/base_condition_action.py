class BaseConditionAction(object):
    match_Lkey = None
    match_Rkey = None
    lvalue = ""
    rvalue = ""

    def __init__(self, **kwargs):
        self.lvalue = kwargs.get("lval", "")
        self.rvalue = kwargs.get("rval", "")