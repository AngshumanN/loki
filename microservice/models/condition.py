from microservice.constants import element_type_key_mapping
from microservice.models.base_condition_action import BaseConditionAction


class Condition(BaseConditionAction):
    operator = "=="
    type = None

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.operator = kwargs.get("op", "==")
        self.type = kwargs.get("type", "==")

    def create_condition_string(self):
        # query_str = '(name == "patient_first_name" and value == "Linda") or (name == "sex" and value == "Female")'
        if self.operator in ["==", ">", "<", "!=", ">=", "<="]:
            if isinstance(self.rvalue, str):
                cond_string = '{match_Lkey} == "{lvalue}" and {match_Rkey} {op} "{rvalue}"'.format(
                    match_Lkey=self.match_Lkey,
                    lvalue=self.lvalue,
                    match_Rkey=self.match_Rkey,
                    op=self.operator,
                    rvalue=self.rvalue)
            else:
                cond_string = '{match_Lkey} == "{lvalue}" and {match_Rkey} {op} {rvalue}'.format(
                    match_Lkey=self.match_Lkey,
                    lvalue=self.lvalue,
                    match_Rkey=self.match_Rkey,
                    op=self.operator,
                    rvalue=self.rvalue)
        elif self.operator.lower() == "contains":
            cond_string = '{match_Lkey} == "{lvalue}" and {match_Rkey}.str.contains(@rvalue)'.format(
                match_Lkey=self.match_Lkey,
                lvalue=self.lvalue,
                match_Rkey=self.match_Rkey,
                op=self.operator)
        else:
            cond_string = ""
        return cond_string

    def execute_condition(self, inputs_df):
        df_list = []

        if self.operator in ["==", ">", "<", "!=", ">=", "<=", "contains"]:
            rvalue = str(self.rvalue)
            if self.type.lower() in ["sentence", "paragraph", "heading", "attribute"]:
                self.match_Lkey, self.match_Rkey = element_type_key_mapping[self.type.lower()]
                query_str = self.create_condition_string()
                df_dict = inputs_df.query(query_str).T.to_dict().values()
                df_list = list(df_dict)

            elif self.type.lower() == "field":
                field_query = 'node_type == "field"'
                field_df = inputs_df.query(field_query).T.to_dict().values()
                field_list = field_df
                for field in field_list:
                    if self.operator == "contains":
                        if field["key"]["text"] == self.lvalue and self.rvalue in field["value"]["text"]:
                            df_list.append(field)
                    else:
                        eval_str = '"{rkey}"{op}"{rval}"' if isinstance(self.rvalue, str) else '{rkey}{op}{rval}'
                        if field["key"]["text"] == self.lvalue and eval(
                                eval_str.format(rkey=field["value"]["text"], op=self.operator,
                                                rval=self.rvalue)):
                            df_list.append(field)

        if df_list:
            return True, df_list
        else:
            return False, []
