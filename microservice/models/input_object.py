from datetime import datetime
import json

import pandas as pd
from jsonpath_rw import parse

from microservice.constants import JSONPATHS
from microservice.utilities.document_utils import flatten_sections


class InputObject(object):
    json_path = None
    input_type = None
    action_type = None
    input_source = None
    file_path = None
    doc_id = None
    json_object = {}
    input_dataframe = None

    def __init__(self, input):
        self.json_path = input.get("json_path", None)
        self.input_source = input.get("input_source", None)
        self.file_path = input.get("file_path", None)
        self.doc_id = input.get("doc_id", None)
        self.input_type = input.get("input_type", None)
        self.action_type = input.get("action_type", None)

        if not self.json_path:
            if self.input_type:
                if self.input_type.lower() in ['domain', 'document']:
                    self.json_path = JSONPATHS[self.input_type.lower()]

        self.get_input_object()

    def get_input_object(self):
        """
        gets the input object from various sources
        :return:
        """
        if self.input_source == "file":
            if self.file_path:
                with open(self.file_path) as f:
                    self.json_object = json.load(f)

    def get_match_value(self):
        """
        gets the matched value from the input object after running the jsonpath
        matching and also converts the result to a dataframe

        :return:
        """
        if self.json_path:
            jsonpath_expression = parse(self.json_path)
            self.matched_value = jsonpath_expression.find(self.json_object)

        matched_list = [m.value for m in self.matched_value]
        st = datetime.now()

        if self.input_type == 'document':
            temp_df = pd.DataFrame(matched_list)
            sections_df = temp_df.query('node_type == "section"').T.to_dict().values()
            sections_list = list(sections_df)
            childs = []
            flatten_sections(childs, sections_list)
            matched_list.extend(childs)

        self.input_dataframe = pd.DataFrame(matched_list)

        end = datetime.now()
        print(len(matched_list))
        print(end - st)

        return self.input_dataframe
