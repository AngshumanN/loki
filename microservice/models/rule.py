from collections import deque

from microservice.models.action import Action
from microservice.utilities.operator_utils import handle_logical_operators, handle_condition_add


class Rule(object):
    conditions = []
    actions = []
    exec_results = deque()
    exec_oper = deque()
    matched_results = []
    input_object = {}
    rule_json = {}

    def __init__(self, rules):
        self.rule_json = rules
        conditions = rules.get("conditions", [])
        actions = rules.get("actions", [])

        for cond in conditions:
            if isinstance(cond, list):
                conditions_list = []
                for c_item in cond:
                    conditions_list.append(handle_condition_add(c_item))
                self.conditions.append(conditions_list)
            else:
                self.conditions.append(handle_condition_add(cond))

        for action in actions:
            action.update()
            ac = Action(**action)
            self.actions.append(ac)

    def apply_action(self):
        for action in self.actions:
            action.perform(self.input_object, self.matched_results)
        return self.input_object

    def exec_cond(self, cond, inputs):
        if not isinstance(cond, dict):
            result, result_list = cond.execute_condition(inputs)
            if len(self.exec_oper) > 0 and len(self.exec_results) > 0:
                prev_res = self.exec_results.popleft()
                result = handle_logical_operators(prev_res, result, self.exec_oper.popleft())
            self.exec_results.appendleft(result)
            if result_list:
                self.matched_results.extend(result_list)
        else:
            self.exec_oper.appendleft(cond['log'])

    def execute(self, inputs):
        for condition in self.conditions:
            if isinstance(condition, list):
                for cond in condition:
                    self.exec_cond(cond, inputs)
            else:
                self.exec_cond(condition, inputs)
        return self.exec_results.popleft()

    def as_json(self):
        return self.rule_json
