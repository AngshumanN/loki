
def flatten_sections(child_list, sections):
    for section in sections:
        if "children" in section:
            for child in section["children"]:
                if child["node_type"] != 'section':
                    child_list.append(child)
                else:
                    flatten_sections(child_list, [child])