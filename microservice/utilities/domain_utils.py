def get_all_parent_ids(matches):
    parent_ids = []
    for match in matches:
        ids = [m["parent_id"] for m in match["matched_value"]]
        parent_ids.append(ids)
    return parent_ids


def check_parents(parent_ids):
    return list(set.intersection(*map(set, parent_ids)))
