from microservice.models.condition import Condition


def handle_logical_operators(val1, val2, operator):
    result = val1
    if operator == "and":
        result = val1 and val2
    elif operator == "or":
        result = val1 or val2
    return result


def handle_condition_add(condition):
    if "log" in condition:
        return condition
    else:
        return Condition(**condition)
