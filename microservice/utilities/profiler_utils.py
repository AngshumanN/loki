from datetime import datetime
import os
import shutil
from pyinstrument import Profiler, renderers

PROFILING_ENABLED = True
PROJECT_NAME = "Rules"


def start_profiler():
    global profiler_file_path
    today = datetime.now()
    file_name = today.strftime('%H:%M:%S:%f.html')
    profiler_file_path = os.path.join("profiling", today.strftime('%Y_%m_%d'), PROJECT_NAME, file_name)
    profiler = Profiler()
    profiler.start()
    return profiler


def stop_profiler(profiler):
    profiler.stop()
    tmp_path = renderers.HTMLRenderer(timeline=False, show_all=False).open_in_browser(profiler.last_session)
    # output_path = profiler_file_path
    # os.mkdir("profiling")
    shutil.copyfile(tmp_path, "profiling/output.html")
    # img_local.copy(output_path)
    # img_local.delete()


def add_profiling(func):
    def inner1(*args, **kwargs):
        if PROFILING_ENABLED:
            profiler = start_profiler()
            trigger = func(*args, **kwargs)
            stop_profiler(profiler)
        else:
            global profiler_file_path
            profiler_file_path = None
            func(*args, **kwargs)

    return inner1
